package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Utils.Assets;

/**
 * Created by X-clio on 25.04.2017.
 */

public class Challenger extends StdScreen {
    public Challenger(SpriteBatch batch) {
        super(batch);
        Button button;
        Label label;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;


        img= new Image(Assets.get().images.get("bgspace"));
        stage.addActor(img);


        button = new TextButton("War", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("War"));
        layout.add(button).size(150).pad(20);

        button = new TextButton("ButtonClicker", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("ButtonClicker"));
        layout.add(button).size(150).pad(20);

        button = new TextButton("Race", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("Race"));
        layout.add(button).size(150).pad(20);

        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add(button).size(150).pad(20);

        ui.addActor(layout);


    }
}
