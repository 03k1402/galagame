package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Utils.Assets;



/**
 * Created by 03k1402 on 04.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Button button;
        Label label;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;


        img= new Image(Assets.get().images.get("bgspace"));
        stage.addActor(img);

        label = new Label("ALPHA v0.0.9", new Label.LabelStyle(Assets.get().fonts.get("text"), Color.WHITE));
        label.setPosition(820, 0);
        stage.addActor(label);

        button = new TextButton("START", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("Challenger"));
        layout.add(button).size(150).pad(20);

        button = new TextButton("Settings", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("Settings"));
        layout.add(button).size(150).pad(20);

        button = new TextButton("Help", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text menu button"));
        button.addListener(new ScreenTraveler("Help"));
        layout.add(button).size(150).pad(20);






        ui.addActor(layout);


    }
}