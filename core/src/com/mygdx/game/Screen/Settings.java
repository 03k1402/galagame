package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 03k1402 on 11.04.2017.
 */
public class Settings extends StdScreen  {
    public Settings(SpriteBatch batch) {
        super(batch);
        Button button;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;


        img= new Image(Assets.get().images.get("bgspace"));
        stage.addActor(img);

        layout.add().expandX();

        img= new Image(Assets.get().images.get("Voiume"));
        img.setPosition(300, 120);
        stage.addActor(img);

        img= new Image(Assets.get().images.get("Sound Line"));
        img.setPosition(380, 140);
        stage.addActor(img);

        img= new Image(Assets.get().images.get("Sound"));
        img.setPosition(300, 360);
        stage.addActor(img);

        img= new Image(Assets.get().images.get("Sound Line"));
        img.setPosition(380, 380);
        stage.addActor(img);

        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text button"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add(button);
        layout.row();

        layout.add().colspan(2).expand().row();

        ui.addActor(layout);
    }

    }

