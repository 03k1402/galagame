package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Model_war.Mob_War;
import com.mygdx.game.Model_war.PlayerController_War;
import com.mygdx.game.Model_war.World_War;
import com.mygdx.game.Utils.Assets;

/**
 * Created by X-clio on 18.04.2017.
 */

public class War extends StdScreen {
    PlayerController_War controller;
    private Timer timer;
    private World_War world;
    public War(SpriteBatch batch) {
        super(batch);
        Button button;
        Label label;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;



        img = new Image(Assets.get().images.get("FonWar"));
        staticBg.addActor(img);


        layout.add().expandX();

        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text button"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add(button);
        layout.row();
        layout.add().colspan(2).expand().row();
        ui.addActor(layout);


        world = new World_War(stage.getViewport(), stage.getBatch());
        stage = world;
        controller = new PlayerController_War(((World_War) stage).getPlayerWar());
        ui.addActor(controller);

        layout.setFillParent(true);

        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("small green shoot"));
        button.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                shootbutton();
            }
        });
        layout.add(button).expandX().right().bottom().padRight(20).padBottom(20).row();

        ui.addActor(layout);
    }

    private void shootbutton() {
        world.shoot();
    }


    public void show(){
        super.show();
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                ((World_War) stage).addRandomMonster();
            }
        }, 0, 4 );



    }
    }


