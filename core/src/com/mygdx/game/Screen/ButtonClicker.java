package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Galagame;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 03k1402 on 11.04.2017.
 */
public class ButtonClicker extends StdScreen {
    private int score;
    private Button button;
    private Label label;
    private Image img;
    private Timer timer;

    public ButtonClicker(SpriteBatch batch) {
        super(batch);
        score = 0;

        Table layout = new Table();
        layout.setFillParent(true);

        stage.addActor(new Image(Assets.get().images.get("ButtonClickerFon")));


        layout.add().expandX();

        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text button"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add(button);
        layout.row();

        button = new TextButton("Clicked", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("Clicked button"));
        button.setPosition(400, 600);
        button.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                hit();
            }
        });
        layout.add(button).colspan(2).expand().row();

        ui.addActor(layout);


        label = new Label("", new Label.LabelStyle(Assets.get().fonts.get("Number"), Color.WHITE));
        label.setPosition(470, 500);
        stage.addActor(label);

    }

    private void hit() {
        score++;
        label.setText(String.format("%d", score));

    }

    public void hide() {
        super.hide();
        ((ButtonClickerResult) Galagame.get().getScreen("ButtonClickerResult")).setScore(score);
    }

    public void postAct() {
        if (timer.isEmpty()) {
            Galagame.get().setScreen("ButtonClickerResult");
        }

    }
    public void show(){
        super.show();
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {

            }
        }, 0, 5, 1);
        timer.start();

    }


}
