package com.mygdx.game.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.game.Utils.Values;

/**
 * Created by ga_nesterchuk on 25.02.2017.
 */
public class StdScreen implements Screen { //класс меню, который является экраном
    protected OrthographicCamera camera; //камера, которая будет смотреть на сцену
    protected OrthographicCamera uicamera; //камера, которая будет смотреть на геймплей
    protected OrthographicCamera staticBgCamera;
    protected Stage stage; //сама сцена
    protected Stage ui; //интерфейс - наши кнопки
    protected Stage staticBg;
    protected InputMultiplexer multiplexer;

    public StdScreen(SpriteBatch batch) {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //создаем камеру и задаем размеры
        camera.setToOrtho(false); //направляем ось Oy вниз
        uicamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //создаем камеру и задаем размеры
        uicamera.setToOrtho(false); //направляем ось Oy вниз
        staticBgCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        staticBgCamera.setToOrtho(false);

        stage = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, camera), batch); //создаем сцену с камерой и отрисовщиком
        ui = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uicamera), batch); //создаем сцену с камерой и отрисовщиком
        staticBg = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, staticBgCamera), batch);
    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer); //все нажатия на libGDX Будет обрабатывать сцена этого экрана
    }

    protected void draw(){
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //закрашиваем экран белым цветом
        staticBg.draw();
        stage.draw(); //отрисовать сцену
        ui.draw();

    }

    protected void act(float delta){
        ui.act(delta);
        stage.act(delta); //сдвинуть всех актеров на сцене
    }

    protected void postAct(){
    }

    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null); //сцена прекращает обработку нажатия
    }

    @Override
    public void dispose() {
        stage.dispose(); // при умирании экрана убить сцену
    }
}