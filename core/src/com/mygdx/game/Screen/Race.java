package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Model_Race.World_Race;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 03k1402 on 16.05.2017.
 */
public class Race extends StdScreen {
    public Race(SpriteBatch batch) {
        super(batch);
    }

    public void show() {

        Button button;
        Label label;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;

        img = new Image(Assets.get().images.get("RaceFon"));
        staticBg.addActor(img);

        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text button"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add(button);
        layout.row();
        layout.add().colspan(2).expand().row();
        ui.addActor(layout);

        stage = new World_Race(stage.getViewport(), stage.getBatch());
        super.show();
    }


}
