package com.mygdx.game.Screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Utils.Assets;
import com.mygdx.game.Utils.Values;

/**
 * Created by 03k1402 on 25.04.2017.
 */
public class ButtonClickerResult extends StdScreen {
    private int score;

    public ButtonClickerResult(SpriteBatch batch) {
        super(batch);
    }


    public void setScore(int score) {
        this.score = score;
    }

    public void show(){
        super.show();


        Label label;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;


        img = new Image(Assets.get().images.get("ResultFon"));
        stage.addActor(img);

        Button button;
        button = new TextButton("Back", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("text button"));
        button.addListener(new ScreenTraveler("Menu"));
        button.setPosition(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, Align.topRight);
        stage.addActor(button);

        label = new Label(String.format("Score : %d", + score), new Label.LabelStyle(Assets.get().fonts.get("red shaded"), Color.WHITE));
        label.setPosition(200, 300);
        stage.addActor(label);

        if (score <= 20) {
            img = new Image(Assets.get().images.get("Bronze"));
            img.setPosition(850, 200);
            stage.addActor(img);

            label = new Label("Title: weakly", new Label.LabelStyle(Assets.get().fonts.get("red shaded"), Color.WHITE));
            label.setPosition(200, 200);

            stage.addActor(label);
        } else if (score >= 60) {
            img = new Image(Assets.get().images.get("Gold"));
            img.setPosition(850, 100);
            stage.addActor(img);

            label = new Label("Title: COOL MAN", new Label.LabelStyle(Assets.get().fonts.get("red shaded"), Color.WHITE));
            label.setPosition(200, 200);
            stage.addActor(label);
        } else {
            img = new Image(Assets.get().images.get("Serebro"));
            img.setPosition(850, 200);
            stage.addActor(img);

            label = new Label("Title: Normal boy", new Label.LabelStyle(Assets.get().fonts.get("red shaded"), Color.WHITE));
            label.setPosition(200, 200);
            stage.addActor(label);
        }
    }

}