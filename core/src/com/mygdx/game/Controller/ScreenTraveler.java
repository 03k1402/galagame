package com.mygdx.game.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Galagame;


/**
 * Created by 03k1402 on 04.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    public String screenName;

    public ScreenTraveler(String screenName) {
        this.screenName = screenName;
    }

    public void clicked(InputEvent event, float x, float y) {
        Galagame.get().setScreen(screenName);
    }
}
