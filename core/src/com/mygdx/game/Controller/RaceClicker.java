package com.mygdx.game.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Model_Race.Player_Race;
import com.mygdx.game.Model_Race.RaceButton;

/**
 * Created by Admin on 23.05.2017.
 */

public class RaceClicker extends ClickListener{
    private RaceButton button;
    private Player_Race player;
    public RaceClicker(RaceButton button, Player_Race player){
        this.button = button;
        this.player = player;
    }
    public void clicked(InputEvent event, float x, float y){
        click();
    }
    public void click(){
        if (button.isChecked()) {
            player.step();
        }
        button.setChecked(false);
        button.unCheck();
    }

}
