package com.mygdx.game.Model_Race;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Screen.StdScreen;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 03k1402 on 16.05.2017.
 */
public class World_Race extends Stage {
    private Player_Race playerRace;
    private int cheked;

    public World_Race(Viewport viewport, Batch batch) {
        super(viewport, batch);
        TextureRegionDrawable[] frames = new TextureRegionDrawable[2];
        for (int i = 0; i < 2; i++)
            frames[i] = Assets.get().images.get(String.format("monsterfly%d", i));
        playerRace = new Player_Race(frames);
        playerRace.setPosition(100, 300);
        addActor(playerRace);

        RaceButton button = new RaceButton(Assets.get().buttonStyles.get("race button"), playerRace, this, 1);
        button.setPosition(100, 100);
        addActor(button);

        button = new RaceButton(Assets.get().buttonStyles.get("race button"), playerRace, this, 2);
        button.setPosition(400, 100);
        addActor(button);
    }

    public boolean canBeChecked() {
        return (cheked == 0);
    }

    public void check(int id){
        cheked = id;
    }

    public void uncheck(){
        cheked = 0;
    }

    public boolean isChecked(int id) {
        return (cheked == id);
    }
}
