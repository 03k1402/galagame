package com.mygdx.game.Model_Race;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Controller.RaceClicker;

import java.util.Random;

/**
 * Created by Admin on 23.05.2017.
 */

public class RaceButton extends Button {
    private Timer timer;
    private static Random r = new Random();
    private World_Race world;
    private int id;

    public RaceButton(ButtonStyle style, Player_Race player, final World_Race world, final int id) {
        super(style);
        this.world = world;
        this.id = id;
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (r.nextBoolean()){
                    if (world.canBeChecked()) {
                        setChecked(true);
                        world.check(id);
                    }
                }
            }
        }, 0, 1f);
        addListener(new RaceClicker(this, player));
    }

    public void unCheck() {
        if (world.isChecked(id)) world.uncheck();
    }
}
