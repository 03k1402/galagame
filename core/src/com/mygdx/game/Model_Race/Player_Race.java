package com.mygdx.game.Model_Race;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g3d.particles.influencers.RegionInfluencer;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by 03k1402 on 16.05.2017.
 */
public class Player_Race extends Image{
    float speed;
    float currentTime;
    Animation animation;

    public Player_Race(TextureRegionDrawable[] frames) {
        super(frames[0]);
        this.speed = 10;
        animation = new Animation(0.5f, frames);
    }

    public void act(float delta){
        super.act(delta);
        currentTime += delta;
        setDrawable((TextureRegionDrawable)animation.getKeyFrame(currentTime, true));

    }

    public void step(){
        moveBy(speed, 0);
    }

}
