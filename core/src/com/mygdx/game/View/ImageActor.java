package com.mygdx.game.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by X-clio on 18.04.2017.
 */

public class ImageActor extends Actor {
    private TextureRegion img;
    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x, y);
        setSize(width, height);
        setOrigin(width / 2, height / 2);
    }
    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }
    public ImageActor(TextureRegion img) {
        this(img, 0, 0, img.getRegionWidth(), img.getRegionHeight());
    }
    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }
    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y);
    }
    public ImageActor(Texture img) {
        this(new TextureRegion(img));
    }
    public ImageActor(String filename, float x, float y, float width, float height) {
        this(new Texture(filename), x, y, width, height);
    }
    public ImageActor(String filename, float x, float y) {
        this(new Texture(filename), x, y);
    }
    public ImageActor(String filename) {
        this(new Texture(filename));
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img,
                getX()-getOriginX(), getY()-getOriginY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                getRotation()-90);
    }
}