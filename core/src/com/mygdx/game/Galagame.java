package com.mygdx.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Screen.ButtonClicker;
import com.mygdx.game.Screen.ButtonClickerResult;
import com.mygdx.game.Screen.Challenger;
import com.mygdx.game.Screen.Help;
import com.mygdx.game.Screen.Menu;
import com.mygdx.game.Screen.Race;
import com.mygdx.game.Screen.Settings;
import com.mygdx.game.Screen.StdScreen;
import com.mygdx.game.Screen.War;

import java.util.HashMap;

public class Galagame extends Game {
	private static Galagame instance = new Galagame();
	public static Galagame get() {return instance;}
	private Galagame(){}

	public SpriteBatch batch;
	public HashMap<String, StdScreen> screens;

	@Override
	public void create() {
		batch = new SpriteBatch();
		screens = new HashMap<String, StdScreen>();
		screens.put("Menu", new Menu(batch));
		screens.put("Settings", new Settings(batch));
		screens.put("Help", new Help(batch));
		screens.put("ButtonClicker", new ButtonClicker(batch));
		screens.put("War", new War(batch));
		screens.put("Challenger", new Challenger(batch));
		screens.put("ButtonClickerResult", new ButtonClickerResult(batch));
		screens.put("Race", new Race(batch));

		setScreen("Menu");
	}

	public void setScreen(String name){
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}

	public StdScreen getScreen(String name){
		return screens.get(name);
	}

}
