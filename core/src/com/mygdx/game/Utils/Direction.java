package com.mygdx.game.Utils;

/**
 * Created by 03k1402 on 18.04.2017.
 */
public enum Direction {
    LEFT,
    RIGHT,
    BACK,
    FORWARD,
    NONE
}
