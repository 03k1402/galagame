package com.mygdx.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.io.File;
import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 04.03.2017.
 */

public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initStyles();
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file("/fonts/HelveticaNeue-Medium.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameters.size = 70;
        parameters.shadowColor = Color.LIGHT_GRAY;
        parameters.shadowOffsetX = 1;
        parameters.shadowOffsetY = 2;
        parameters.color = Color.RED;
        parameters.characters = "0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя:";
        fonts.put("red shaded", generator.generateFont(parameters));

        generator = new FreeTypeFontGenerator(file("/fonts/Magneto-Bold.ttf"));
        parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameters.size = 25;
        parameters.shadowColor = Color.LIGHT_GRAY;
        parameters.shadowOffsetX = 1;
        parameters.shadowOffsetY = 2;
        parameters.color = Color.WHITE;
        parameters.characters = " 1234567890";
        fonts.put("Number", generator.generateFont(parameters));

        fonts.put("text", new BitmapFont());

    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("text menu button", new TextButton.TextButtonStyle(
                        images.get("buttonMenu"),
                        images.get("buttonMenuClicked"),
                        null,
                        fonts.get("text")
                )
        );
        buttonStyles.put("Clicked button", new TextButton.TextButtonStyle(
                        images.get("Levelgreen_min"),
                        images.get("Levelreoundred_min"),
                        null,
                        fonts.get("text")
                )
        );
        buttonStyles.put("text button", new TextButton.TextButtonStyle(
                        images.get("back"),
                        null,
                        null,
                        fonts.get("text")
                )
        );
        buttonStyles.put("small green shoot",
                new ImageButton.ImageButtonStyle(
                        images.get("green_button07"),
                        images.get("green_button08"),
                        null,
                        images.get("laserRedShot"),
                        null,
                        null
                )
        );
        buttonStyles.put("race button",
                new Button.ButtonStyle(
                        images.get("Button1"),
                        images.get("Button1"),
                        images.get("Button0")
                )
        );
    }

    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/");
        addImagesFolder("/Ui");
        addImagesFolder("/Shoots");
        addImagesFolder("/Ui/Button");
        addImagesFolder("/Ui/monsterfly");
        for (String name : images.keySet()) System.out.println(name);

    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        Texture texture = new Texture(file);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return new TextureRegionDrawable(new TextureRegion(texture));
    }

    private FileHandle file(String path) {
        return Gdx.files.local(path);
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, BitmapFont> fonts;
}