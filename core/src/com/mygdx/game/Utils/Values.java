package com.mygdx.game.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 03k1402 on 18.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 960;
    public static final float WORLD_HEIGHT = 540;
    public static final float ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
    public static final float ppuY = Gdx.graphics.getWidth()/WORLD_HEIGHT;
}
