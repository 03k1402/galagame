package com.mygdx.game.Model_war;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.Utils.Direction;

import java.util.HashSet;

/**
 * Created by X-clio on 18.04.2017.
 */

public class Player_War extends Image {
    private HashSet<Integer> pressedButtons;
    private float v;
    private int lives;
    private boolean back;
    private boolean forward;
    private boolean left;
    private boolean right;

    public Player_War(TextureRegionDrawable img) {
        super(img);
        v = 100;
        lives = 100;
    }

    public void act(float delta) {
        super.act(delta);
        if (back && !forward) move(v * delta);
        if (!back && forward) move(-v * delta);
        if (left && !right) rotateBy(v * delta);
        if (!left && right) rotateBy(-v * delta);
    }

    public void hit() {
        lives--;
        System.out.println(lives);
    }

    public void move(float destination) {
        moveBy((float) (destination * Math.cos(Math.toRadians(getRotation() - 90))), (float) (destination * Math.sin(Math.toRadians(getRotation() - 90))));
    }

    public void setDirection(Direction direction) {
        switch (direction) {
            case BACK:
                back = true;
                break;
            case FORWARD:
                forward = true;
                break;
            case LEFT:
                left = true;
                break;
            case RIGHT:
                right = true;
                break;
        }
    }

    public void stopDirection(Direction direction) {
        switch (direction) {
            case BACK:
                back = false;
                break;
            case FORWARD:
                forward = false;
                break;
            case LEFT:
                left = false;
                break;
            case RIGHT:
                right = false;
                break;
        }

    }

}

