package com.mygdx.game.Model_war;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.View.ImageActor;

/**
 * Created by X-clio on 18.04.2017.
 */

public class Mob_War extends ImageActor {

    Player_War playerWar;
    float v;
    int livesmob;
    public Mob_War(String filename, float x, float y, Player_War playerWar) {
        super(filename, x, y);
        this.playerWar = playerWar;
        this.v = 80;
        this.livesmob = 1;


    }

    public void act(float delta){
        setRotation(
                (float) Math.toDegrees(
                        Math.atan2(playerWar.getY() - getY(), playerWar.getX() - getX())
                )
        );
        moveBy(
                v * delta * (float) Math.cos(Math.toRadians(getRotation())),
                v * delta * (float) Math.sin(Math.toRadians(getRotation()))
        );
        if (Vector2.dst2(getX(),getY(), playerWar.getX(), playerWar.getY())<v*delta){
            playerWar.hit();
            remove();
        }

    }

    public void shoot() {
        livesmob--;
    }
}

