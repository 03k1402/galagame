package com.mygdx.game.Model_war;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Utils.Assets;

import java.util.HashSet;
import java.util.Random;

/**
 * Created by X-clio on 18.04.2017.
 */

public class World_War extends Stage {
    private Player_War playerWar;
    private Vector2 stageCoords;
    private HashSet<Shoot> shoots;

    public World_War(Viewport screenViewport, Batch batch) {
        super(screenViewport, batch);
        playerWar = new Player_War(Assets.get().images.get("Spaceship"));
        playerWar.setPosition(100, 100);
        playerWar.setSize(50, 50);
        playerWar.setOrigin(Align.center);
        addActor(playerWar);
        stageCoords = new Vector2();
        shoots = new HashSet<Shoot>();
    }

    public Player_War getPlayerWar(){return playerWar;}

    public void addRandomMonster(){
        Random r = new Random();
        int x = r.nextInt(Gdx.graphics.getWidth())-Gdx.graphics.getWidth()/2;
        if (x > 0) x+=Gdx.graphics.getWidth();
        int y = r.nextInt(Gdx.graphics.getHeight())-Gdx.graphics.getHeight()/2;
        if (y > 0) y+=Gdx.graphics.getHeight();
        stageCoords.set(x, y);
        screenToStageCoordinates(stageCoords);
        addActor(new Mob_War("ui/mob.png", stageCoords.x, stageCoords.y, playerWar));
    }

    public void shoot() {
        Shoot shoot = new Shoot(Assets.get().images.get("laserGreenShot"));
        shoot.setRotation(playerWar.getRotation());
        shoot.setPosition(playerWar.getX(), playerWar.getY());
        shoots.add(shoot);
        addActor(shoot);
    }
}






