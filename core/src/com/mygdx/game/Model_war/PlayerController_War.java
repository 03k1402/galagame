package com.mygdx.game.Model_war;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Utils.Assets;
import com.mygdx.game.Utils.Direction;

import static com.mygdx.game.Utils.Direction.*;

/**
 * Created by 03k1402 on 18.04.2017.
 */
public class PlayerController_War extends Group {

    private Player_War player;

    public PlayerController_War(final Player_War player) {
        this.player = player;
        Image img = new Image(Assets.get().images.get("button_down"));
        addActor(img);
        img.addListener(new PlayerMover(BACK));
        img.setPosition(100, 0);

        img = new Image(Assets.get().images.get("button_left"));
        addActor(img);
        img.addListener(new PlayerMover(LEFT));
        img.setPosition(0, 0);

        img = new Image(Assets.get().images.get("button_up"));
        addActor(img);
        img.addListener(new PlayerMover(FORWARD));
        img.setPosition(100, 100);

        img = new Image(Assets.get().images.get("button_right"));
        addActor(img);
        img.addListener(new PlayerMover(RIGHT));
        img.setPosition(200, 0);
    }

    class PlayerMover extends ClickListener {
        private Direction direction;

        public PlayerMover(Direction direction) {
            this.direction = direction;
        }

        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
            super.enter(event, x, y, pointer, fromActor);
            player.setDirection(direction);
        }

        public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
            super.exit(event, x, y, pointer, toActor);
            player.stopDirection(direction);
        }
    }
}
