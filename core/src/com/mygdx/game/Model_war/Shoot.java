package com.mygdx.game.Model_war;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by 03k1402 on 30.05.2017.
 */
public class Shoot extends Image {
    private double angle;
    private double angleC;
    private double angleS;
    private float velocity;

    public Shoot(TextureRegionDrawable drawable) {
        super(drawable);
        velocity = 80;
    }

    public void setRotation(float degree) {
        super.setRotation(degree);
        angle = Math.toRadians(degree);
        angleS = velocity * Math.sin(angle);
        angleC = velocity * Math.cos(angle);
    }

    public void act(float delta) {
        moveBy((float) angleC, (float) angleS);
        super.act(delta);
    }
}
